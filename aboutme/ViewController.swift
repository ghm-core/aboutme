//
//  ViewController.swift
//  aboutme
//
//  Created by VJ Pranay on 13/06/18.
//  Copyright © 2018 Lamda School. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let name = "Pranay"
    var age = "22"
    var city = "Waranagal"
    var textC = "Close"
    
    @IBAction func introduceMe(_ sender: Any) {
        nameDisplay.text = "Name : " + name
        ageDisplay.text =  "Age  : " + age
        cityDisplay.text = "City : " + city
        imageDisplay.image = #imageLiteral(resourceName: "pranay")
        print(name)
        print(age)
        print(city)
    }
    @IBOutlet weak var mainButtonChange: UIButton!
    @IBOutlet weak var nameDisplay: UITextView!
    @IBOutlet weak var ageDisplay: UITextView!
    @IBOutlet weak var cityDisplay: UITextView!
    @IBOutlet weak var imageDisplay: UIImageView!
}

